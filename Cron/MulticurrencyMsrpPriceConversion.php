<?php


namespace M21\MulticurrencyMsrpPriceConversion\Cron;

class MulticurrencyMsrpPriceConversion
{

    protected $settings;

    /**
     * MulticurrencyMsrpPriceConversion constructor.
     * @param \M21\MulticurrencyMsrpPriceConversion\lib\Settings $settings
     */
    public function __construct(
        \M21\MulticurrencyMsrpPriceConversion\lib\Settings $settings

    )
    {
        $this->settings = $settings;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->settings->log->setLog("Cronjob MulticurrencyMsrpPriceConversion is executed.");
    }
}
