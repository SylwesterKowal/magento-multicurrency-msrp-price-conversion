<?php


namespace M21\MulticurrencyMsrpPriceConversion\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Convert extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    protected $settings;
    protected $client;
    protected $currencies;
    protected $products;

    /**
     * Convert constructor.
     * @param \M21\MulticurrencyMsrpPriceConversion\lib\Settings $settings
     * @param \M21\MulticurrencyMsrpPriceConversion\lib\Client $client
     * @param \M21\MulticurrencyMsrpPriceConversion\lib\Currencies $currencies
     * @param \M21\MulticurrencyMsrpPriceConversion\lib\Products $products
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \M21\MulticurrencyMsrpPriceConversion\lib\Settings $settings,
        \M21\MulticurrencyMsrpPriceConversion\lib\Client $client,
        \M21\MulticurrencyMsrpPriceConversion\lib\Currencies $currencies,
        \M21\MulticurrencyMsrpPriceConversion\lib\Products $products
    )
    {
        $this->state = $state;
        $this->settings = $settings;
        $this->client = $client;
        $this->currencies = $currencies;
        $this->products = $products;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

        if ($this->settings->getStatus()) {
            $output->writeln('start');
            $name = $input->getArgument(self::NAME_ARGUMENT);
            $option = $input->getOption(self::NAME_OPTION);

            $this->products->currencies = $this->currencies->parse($this->client->getContent());
            $this->settings->log->setLog(print_r($this->products->currencies, true));

            $this->products->getProductsToUpdate();

            foreach ($this->products->products as $p) {
                $output->writeln('Start SKU:' . $p->getSku());
                $output->writeln('Start CustomMsrpCurrency:' . $p->getResource()->getAttribute('custom_msrp_currency')->getFrontend()->getValue($p));
                $result = $this->products->updateCatalogPrice($p);
                $this->settings->log->setLog('Wykonano: ' . print_r($result, true));
            }


            $output->writeln('koniec');
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("m21:convert");
        $this->setDescription("Konweruj MSRP do Price");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }

}
