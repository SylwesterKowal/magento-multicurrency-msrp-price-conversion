<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 18.08.2018
 * Time: 16:46
 */

namespace M21\MulticurrencyMsrpPriceConversion\lib;

class Logs
{
    public $loggerl;
    public $debug;

    /**
     * Logs constructor.
     * @param Settings $settings
     */
    public function __construct()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/custom_msrp_conversion.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);

    }

    public function setLog($content)
    {
        if ($this->debug)
            $this->logger->info($content);
    }
}