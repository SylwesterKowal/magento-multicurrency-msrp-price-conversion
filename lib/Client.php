<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 18.08.2018
 * Time: 17:18
 */

namespace M21\MulticurrencyMsrpPriceConversion\lib;

use \Magento\Framework\HTTP\Client\Curl;

class Client extends Curl
{

    protected $settings;

    /**
     * Client constructor.
     * @param Settings $settings
     */
    public function __construct(
        \M21\MulticurrencyMsrpPriceConversion\lib\Settings $settings

    )
    {
        $this->settings = $settings;
    }

    /**
     * @param string $ip
     * @return string
     */
    public function getContent($url = false)
    {
        if (!$url) $url = $this->settings->getUrl();

        $this->settings->log->setLog($url);

        $this->_curlUserOptions = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => ["Host: {$this->settings->getHost()}", "Content-Type: application/json"]
        ];

        $this->get($url);
        return json_decode($this->getBody());
    }
}
