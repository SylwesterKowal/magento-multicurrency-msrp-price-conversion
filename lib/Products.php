<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 01.10.2018
 * Time: 01:56
 */

namespace M21\MulticurrencyMsrpPriceConversion\lib;

use \Magento\Catalog\Model\ResourceModel\Product\Collection;

class Products
{

    protected $productCollection;
    public $products;
    public $currencies;


    /**
     * Products constructor.
     * @param Collection $productCollection
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
    )
    {
        $this->productCollection = $productCollection;
    }

    /**
     * Zwraca listę produktów pomijając te dla których są już wystawione aukcje
     * @return mixed
     */
    public function getProductsToUpdate()
    {
        $this->products = $this->productCollection
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('custom_msrp')
            ->addAttributeToSelect('custom_msrp_currency')
            ->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
            ->addAttributeToFilter('custom_msrp_currency', ['neq' => ''])
            ->addAttributeToFilter('custom_msrp', ['gt' => 0])
            ->addAttributeToFilter('custom_msrp_enable', ['eq' => 1])
            ->load();
    }

    public function updateCatalogPrice($product)
    {
        $curr = $product->getResource()->getAttribute('custom_msrp_currency')->getFrontend()->getValue($product);
        if (array_key_exists($curr, $this->currencies)) {
            $cours = $this->currencies[$curr];
            $price = $cours * $product->getCustomMsrp();
            $product->setPrice($price);
            $product->save();
            return ['sku' => $product->getSku(), 'new price: ' => $price, 'kurs: ' => $cours, 'currency' => $curr];
        } else {
            return false;
        }
    }

}