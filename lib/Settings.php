<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 27.09.2018
 * Time: 12:33
 */

namespace M21\MulticurrencyMsrpPriceConversion\lib;


class Settings
{
    protected $scopeConfig;
    public $log;

    /**
     * Settings constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Logs $logs
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \M21\MulticurrencyMsrpPriceConversion\lib\Logs $logs
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->log = $logs;
        $this->log->debug = $this->scopeConfig->getValue('settings/main/debug', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getStatus()
    {
        return $this->scopeConfig->getValue('settings/main/status', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getDebug()
    {
        return $this->scopeConfig->getValue('settings/main/debug', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getHost()
    {
        return $this->scopeConfig->getValue('settings/parametry/host', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getUrl()
    {
        return $this->scopeConfig->getValue('settings/parametry/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}