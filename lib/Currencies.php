<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 01.10.2018
 * Time: 01:33
 */

namespace M21\MulticurrencyMsrpPriceConversion\lib;


class Currencies
{
    public function parse($soure)
    {
        $curr = [];
        if (isset($soure[0]->rates)) {
            foreach ($soure[0]->rates as $c) {
                $curr[$c->code] = $c->mid;
            }
        } else {
            $curr = false;
        }
        return $curr;
    }
}